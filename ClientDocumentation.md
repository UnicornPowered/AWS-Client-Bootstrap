# Client Installation

Cette procédure détails les étapes à suivre pour donner les accès à votre environnement Amazon Web Services (AWS) à l'équipe de Unicorn Powered.

Si vous n'avez pas encore de compte sur AWS, cliquez sur ce lien pour créer votre compte AWS: [https://portal.aws.amazon.com/billing/signup#/start](https://portal.aws.amazon.com/billing/signup#/start)

## Création d'un role **UnicornAccessRole**

### Via AWS Console (Option 1)

Cette procédure détails les étapes nécessaire pour déployer la "Stack" donnant les accès à Unicorn Powered dans votre environnement.

> À tous moment, vous pourrez modifier les accès de ce rôle pour limiter ou retirer entièrement les accès à Unicorn Powered. Si vous désirez retirer les accès à Unicorn Powered, vous n'avez qu'a supprimer la stack.

1. Télécharger le fichier de stack **UnicornAccessRole.yml** à cette adresse: [https://s3.ca-central-1.amazonaws.com/aws-client-ca-central-1/master/UnicornAccessRole.yml](https://s3.ca-central-1.amazonaws.com/aws-client-ca-central-1/master/UnicornAccessRole.yml).

2. Accéder à votre environnement AWS: [https://ca-central-1.console.aws.amazon.com/](https://ca-central-1.console.aws.amazon.com/).

3. Assurez-vous de selectionner la zone **Canada (Central)**.
![image](images/CloudFormation0.png)

3. Cliquer sur **Services** et sélectionner **Cloud Formation**.
![images](images/CloudFormation1.png)

4. Cliquer sur **Create Stack**

5. Dans la section **Upload a template to Amazon S3** cliquer sur **Choose File**. Sélectionner le fichier **UnicornAccessRole.yml**. Cliquer sur **Next**
![images](images/CloudFormation2.png)

6. Entrer un nom pour la stack. Par exemple **UnicornPoweredAccess**. Mais il est possible de mettre un nom différent. Entrer votre nom de compagnie dans le champs **CustomerName** et cliquer sur **Next**.
![images](images/CloudFormation3.png)

7. Cliquer sur **Next**

8. Cocher la case à cocher pour confirmer la possibilité de modifier les resources IAM. Cliquer sur **Next**

9. Attendre que la stack termine sa création. Cliquer sur le bouton **refresh** pour voir l'avancement. Cliquer sur la stack et consulté la section **Outputs**. Il devrait y avoir les trois éléments suivants: **CompagnyName**, **RoleName** et **CustomerAccountId**.
![images](images/CloudFormation4.png)

### Via AWS Console avec lien

https://console.aws.amazon.com/cloudformation/home?region=ca-central-1#/stacks/create/review?stackName=unicorne-access&templateURL=https://s3.ca-central-1.amazonaws.com/aws-client-ca-central-1/master/UnicornAccessRole.yml&param_CompanyName=my-company

### Via cli (Option 3)

1. Télécharger le fichier de stack **UnicornAccessRole.yml** à cette adresse: [https://s3.ca-central-1.amazonaws.com/aws-client-ca-central-1/master/UnicornAccessRole.yml](https://s3.ca-central-1.amazonaws.com/aws-client-ca-central-1/master/UnicornAccessRole.yml).

```bash
wget https://s3.ca-central-1.amazonaws.com/aws-client-ca-central-1/master/UnicornAccessRole.yml
```

2. Creation de la stack
```
aws cloudformation deploy --template-file UnicornAccessRole.yml --stack-name unicorn-access --capabilities CAPABILITY_NAMED_IAM --parameter-overrides CompanyName=TheCompagnyName --profile profile-of-compangy --region ca-central-1
```
